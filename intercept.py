import copy
from qkd import qkSender, qkCommChannel, qkReceiver, convert

    # Entity Creation

Alice = qkSender()
insecure_channel = qkCommChannel()
Bob = qkReceiver()
Eve = qkReceiver()
#Eve = qkAttacker.qkAttacker()

    # Alice creates a randomized pulse composed of photons, she also records the basis for each photon.
    # Alice then sends the photon pulse.
    # Bob then attempts to measure the photon pulse, by randomly generating his own basis for each photon in the pulse.

Alice.create_photon_pulse()
Alice.send_pulse(insecure_channel, copy.deepcopy(Alice.photon_pulse))
#Eve.attack(insecure_channel)
Bob.measure_polarizations(insecure_channel)
Alice.send_pulse(insecure_channel, copy.deepcopy(Alice.photon_pulse))

Eve.measure_polarizations(insecure_channel)

    #Alice sends basis to Bob

Alice.send_pulse_basis(insecure_channel, copy.deepcopy(Alice.random_basis))
#Eve.get_other_basis(insecure_channel)


#Eve.send_pulse_basis(insecure_channel, copy.deepcopy(Eve.random_basis))
Bob.get_other_basis(insecure_channel)


#Eve.send_pulse_basis(insecure_channel, copy.deepcopy(Eve.random_basis))
#Alice.get_other_basis(insecure_channel)

#Bob.send_pulse_basis(insecure_channel, copy.deepcopy(Bob.random_basis))
#Eve.get_other_basis(insecure_channel)
    #Bob sends basis to Alice

Bob.send_pulse_basis(insecure_channel, copy.deepcopy(Bob.random_basis))
Alice.get_other_basis(insecure_channel)

    #Alice and bob compare basis to see if a certain # of shared bits was reached.

Alice.drop_invalid_polars()
Bob.drop_invalid_polars()
#Eve.drop_invalid_polars
    #Alice and Bob both break their polarizations down into bit strings/shared_key

Alice.set_bit_string()
Bob.set_bit_string()
Eve.set_bit_string()

    #Alice and Bob compare sub set of the bit string (photon polarizations -> bits)

Alice.send_pulse_sub_bit_string(insecure_channel)
Bob.get_sub_bit_string(insecure_channel)
Bob.send_pulse_sub_bit_string(insecure_channel)
Alice.get_sub_bit_string(insecure_channel)

    #Alice and Bob decide if each others sub shared keys are "equal" enough to trust the channel and declare a shared key

Alice.decide()
Bob.decide()

    #Alice and Bob send_pulse there decisions on whether the shared key is valid, by send_pulseing the decision to each other

Alice.send_pulse_decision(insecure_channel)
Bob.get_decision(insecure_channel)
Bob.send_pulse_decision(insecure_channel)
Alice.get_decision(insecure_channel)

    #Alice and Bob can now try again in a new channel or use the shared key based on the decision

final = convert([Alice.shared_key, Bob.shared_key])
print('Final binary string : ', final)

print("Intercept & Resend")
print("Alice's Key after shifting", Alice.shared_key)
print("Bob's Key after shifting ", Bob.shared_key)
print("Eve's Key after interpretation ", Eve.shared_key)

def compare(key_a, key_b):
    print(len(key_a), len(key_b))
    if len(key_a) < len(key_b):
        key_b = [key_b[i] for i in range(0, len(key_a))]
    elif len(key_b) < len(key_a):
        key_a = [key_a[i] for i in range(0, len(key_b))]
    else:
        pass
    res = [1 if key_a[i]==key_b[i] else 0 for i in range(0, len(key_a))]
    return sum(res)/float(len(res))
print("Key Match Ratio For Alice & Bob")
print(compare(Alice.shared_key, Bob.shared_key))
print("Key Match Ratio For Alice & Eve")
print(compare(Alice.shared_key, Eve.shared_key))

if Alice.valid_key == 1 and Bob.valid_key == 1:
    if Alice.shared_key == Bob.shared_key:
        print("Alice & Bob's Shared Secret Key:  ", Alice.shared_key)
        print("Length Of Shared Secret Key:     ", len(Alice.shared_key))
    else:
        print("Error in implementation (if no attacker set/req # of sub shared reached)")
else:
    print("Alice & Bob have not agreed upon a Shared Secret Key")
