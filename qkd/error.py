from numpy import array, concatenate, split
from numpy.core.defchararray import add, equal

def XOR_seq(arr):
    ''''''
    res = int(arr[0])
    for i in range(1, len(arr)):
        res = res ^ int(arr[i])
    return str(res)

def compare(arr):
    ''''''
    arr = array([list(e) for e in arr])
    bool_arr = equal(arr[0], arr[1])
    if False not in bool_arr:
        return -1
    else:
        return bool_arr.tolist().index(False)

def convert(string):
    ''''''
    string = [''.join([str(f) for f in e]) for e in string]
    print string
    max_range = len(string[0])
    if max_range%8 == 0:
        max_range /= 8
    else:
        max_range = max_range/8 + 1
    split_arr = [8*i for i in range(1, max_range)]
    arr = array([split(array(list(e)), split_arr) for e in string])
    out = array(['', ''])
    for i in range(0, len(arr[0])):
        out = add(out, array([XOR_seq(arr[0][i]), XOR_seq(arr[1][i])]))
    print out
    index = compare(out)
    if index == -1:
        max_final = len(string[0])
        final = split(list(string[0]), [2*i for i in range(1, max_final/2 + max_final%2)])
        out_final = array([''])
        for i in range(0, len(final)):
            out_final = add(out_final, array(XOR_seq(final[i])))
        return out_final
    else:
        index_diff = compare([arr[0][index], arr[1][index]])
        arr[0][index] = arr[0][index][index_diff+1:]
        final = arr[0][0]
        for i in range(1, len(arr[0])):
            final = concatenate((final, arr[0][i]))
        return ''.join(final)
